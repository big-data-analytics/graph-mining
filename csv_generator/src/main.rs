mod db_reader;

use std::{
    collections::HashSet,
    fs::File,
    io::{prelude::*, BufWriter},
};

pub const CSV_PATH: &str = "../pods_edges.csv";
pub const CSV_FULL_PATH: &str = "../all_edges_pods_authors.csv";
pub const PODS: &str = "pods";
pub const PODS_UP: &str = "PODS";

fn write_book(
    book: &db_reader::Book,
    mut bufr: BufWriter<File>,
) -> Result<BufWriter<File>, Box<dyn std::error::Error>> {
    for (index, author0) in book.authors.iter().enumerate() {
        for author1 in book.authors.iter().skip(1).skip(index) {
            bufr.write(&book.year.unwrap().to_string().into_bytes())?;
            bufr.write(b",")?;
            bufr.write(author0)?;
            bufr.write(b",")?;
            bufr.write(author1)?;
            bufr.write(b"\n")?;
        }
    }

    Ok(bufr)
}

fn print_pods(
    book: &db_reader::Book,
    pods_authors: &HashSet<Vec<u8>>,
    output_file: &mut BufWriter<File>,
) -> Result<(), Box<dyn std::error::Error>> {
    for (index, author0) in book.authors.iter().enumerate() {
        for author1 in book.authors.iter().skip(1).skip(index) {
            if pods_authors.contains(author0) && pods_authors.contains(author1) {
                output_file.write(
                    &book
                        .year
                        .and_then(|book| Some(book.to_string().into_bytes()))
                        .unwrap_or(b"unknown".to_vec()),
                )?;
                output_file.write(b",")?;
                output_file.write(author0)?;
                output_file.write(b",")?;
                output_file.write(author1)?;
                output_file.write(b"\n")?;
            }
        }
    }
    Ok(())
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut pods_authors: HashSet<Vec<u8>> = HashSet::new();
    let input_file = db_reader::BooksReader::from_path(db_reader::DEFAULT_DB_PATH).unwrap();
    let pods_output = std::fs::File::create(CSV_PATH).unwrap();
    let mut pods_buffer = BufWriter::new(pods_output);

    let input_file2 = db_reader::BooksReader::from_path(db_reader::DEFAULT_DB_PATH).unwrap();
    let full_output = std::fs::File::create(CSV_FULL_PATH).unwrap();
    let mut full_output = BufWriter::new(full_output);

    for book in input_file {
        match book {
            Ok(book) => {
                if book.year.is_some() && !book.authors.is_empty() {
                    let booktitle_contains = (&book.booktitle).is_some() && {
                        let booktitle =
                            String::from_utf8_lossy(&(book.booktitle.as_ref()).unwrap());
                        booktitle.contains(PODS_UP)
                    };
                    let note_contains = match &book.note {
                        Some(note) => {
                            let note = String::from_utf8_lossy(&note);
                            note.contains(PODS)
                        }
                        _ => false,
                    };

                    if booktitle_contains || note_contains {
                        pods_buffer = write_book(&book, pods_buffer)?;
                        for author in book.authors {
                            pods_authors.insert(author.clone());
                        }
                    }
                } else {
                    // Terminal output was slowing us down
                    // if let Some(strg) = book.title {
                    //     if strg != b"Home Page" {
                    //         println!(
                    //             "Warning: ignoring {} because it has no year or booktitle (topic) \
                    //              specified.",
                    //             String::from_utf8(strg).unwrap()
                    //         );
                    //     }
                    // }
                }
            }
            Err(err) => eprintln!("{}", err),
        }
    }

    // Read full dataset with pods authors on publications outside of the conference.
    for book in input_file2 {
        if let Ok(book) = book {
            match print_pods(&book, &pods_authors, &mut full_output) {
                Err(any) => {
                    println!("Error: {:?}", any);
                }
                _ => {}
            }
        } else {
            println!("Error: {:?}", book);
        }
    }

    Ok(())
}
