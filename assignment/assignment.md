# Assignment

The [ACM Symposium on Principles of Database Systems (PODS)](http://dblp.uni-trier.de/db/conf/pods/index.html) is the main database theory conference. An [analysis on this community](http://dl.acm.org/citation.cfm?doid=2070736.2070749) was done in 2011 on the occasion of the 30th anniversary of the conference.

The assignment consists of two parts:

1. Compute the betweenness centrality of researchers (as in Section 4.1 of [this paper](http://dl.acm.org/citation.cfm?doid=2070736.2070749)) over different time periods (for instance, five or 10 years).

1. Compute communities within the PODS graph for the time period 2006-2016. (Have a look at Section 4.3 of this [paper](http://dl.acm.org/citation.cfm?doid=2070736.2070749) and at this [figure](https://sigmod.hosting.acm.org/sigmodnewwp/wp-content/uploads/2015/07/cliques.pdf)).

You should hand in your code together with a report containing the results of your analysis and an explanation of the followed approach and the difficulties you encountered.

## Alternative topic

Apply graph mining to the coding submissions of the plagiarism detection assignment. Visualize your results.

Tip: [SNAP library](https://snap.stanford.edu/)


Suggested deadline: 20/12/2019. Real deadline: 10/01/2020.

