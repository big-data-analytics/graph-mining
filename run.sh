#!/bin/bash

cd csv_generator
rustup override set 1.39.0
cargo run --release
cd ..

pip3 install -r requirements.txt
python3 src/betweenness.py
python3 src/communities.py
