# How to use

Run the `run.sh`-script, or follow the steps below.

1. Compile the Rust parser and use it to generate a csv with co-authors.

   ```bash
   cd csv_generator
   # To ensure compatibility
   rustup override set 1.39.0
   cargo run --release
   cd ..
   ```

   This will generate `inputdata.csv`: a file with the following format: `year,coAuthor1,coAuthor2`. If a book has more than 2 authors, all pairs will appear in this file, in order to lift as much preprocessing work as possible from Python into Rust.

1. Make sure you have all the required dependencies installed. It goes without saying that a Python 3 interpreter is required (tested using CPython 3.8.0). If you use `pip` as your package manager, you can install them using

   ```bash
   pip install -r requirements.txt
   ```

   Using pacman (on Arch Linux):

   ```bash
   sudo pacman -S python-networkx python-matplotlib
   ```

1. Run `betweenness.py` to calculate betweenness over the different intervals. This should be run from the project root directory.
   
   ```bash
   python3 src/betweenness.py
   ```

1. Run `communities.py` to calculate communities.
   
   ```bash
   python3 src/communities.py
   ```
