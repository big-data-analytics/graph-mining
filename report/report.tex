\documentclass[a4paper]{article}

\usepackage{biblatex}
\addbibresource{sources.bib}
\usepackage{graphicx}
\usepackage{pdflscape}

\title{Big Data Analytics\\Graph Mining}
\author{Mihály Csonka\\1644219\and Ward Segers\\1642793}
\date{10 January 2020}

\begin{document}

\maketitle

\section{Dataset preparation}

For this assignment we filtered the DBLP dataset for authors who published under the PODS conference. This was done by looking for publications with the PODS keyword in their \texttt{booktitle} or \texttt{note} XML element. All the authors present in these publications are our `PODS authors'.

We then searched the DBLP dataset again for any publications made by a group of only these authors\footnote{If authors A, B and C wrote a paper together and A and B are PODS authors, we do not count C but we do count the node between }, even when the publication wasn't made under the PODS conference. For each publication we then cached all unique pair combinations of its authors along with the publication's year.

\section{Dependencies}
For both tasks we made full use of the capabilities of the \texttt{NetworkX} library \cite{networkx}. This library wasn't the one given as tip for this assignment, but it has pretty much the same functionality as the SNAP library \cite{snap}. \texttt{NetworkX} allowed us to calculate the betweenness as well as the k-clique communities with ease.

\section{Betweenness centrality}

The betweenness centrality measures the proportion of all shortest paths which pass through a node. In other words, a author with a high centrality score has cooperated with many others resulting in them being the common link between these others.

As assigned, for calculating this measure we mimicked the given paper. That is, the graph contains author pairs for publications made in the years up till and including a given end-year.

\subsection{Observations}\label{betweenness}

The first few years, that is begin 1970's, the dataset is quite sparse. One of the first authors appearing is J.D. Ullman, who will go on to be a pretty active contributor throughout each epoch.

The second half of the 1970's starts to see more registered publications. The amount of connected authors also rapidly increases. In 1980 the graph is already tightly connected, and will only become more and more so as time goes on.

Around the same time, we see another busy author appear on the `betweenness centrality leader board', namely C.H. Papadimitriou. They start and stay in the top 3 from 1980 till around 1986. Even after that, they will stay in the top 10 every year, just like Ullman.

It is in 1987, again as if butting the previous top from their pedestal, that we see our all time collaborator appearing. S. Abiteboul quickly takes the top in the latter part of the 1980's. However, it takes ups and downs in the next decade before they firmly establish themselves as top 1 collaborator starting in the 2000's. It does not surprise us to see Ullman and Papadimitriou in the top during the moody decade of 1990.

A few honourable mentions, authors who appear quite a lot in the top 10 each year, along with the amount of appearances are:
\begin{itemize}
    \item A. Silberschatz: 22
    \item D. Maier: 21
    \item K.A. Ross: 19
    \item R. Ramakrishnan: 17
    \item D. Suciu: 16
    \item N.A. Lynch: 15
    \item M.Y. Vardi: 15
\end{itemize}
For reference: Ullman appears 52 times, Papadimitriou 32 times, and Abiteboul 29 times.

A last thing to look at would be the current standings. As of the end of 2019 the top 10 collaborating authors, by order of decreasing betweenness centrality score, are:
\begin{enumerate}
    \item S. Abiteboul
    \item D. Suciu
    \item D. Maier
    \item J.D. Ullman
    \item T. Milo
    \item R. Ramakrishnan
    \item K.A. Ross
    \item S. Muthukrishnan
    \item L. Libkin
    \item H.V. Jagadish
\end{enumerate}

\section{Communities}

We also looked for communities of authors. A community is a tightly knit group of authors who collaborated on publications. As mentioned before, it doesn't matter if these publications are for the PODS-conferences.

People within a community tend to work together more often, as they know each other well. This is why we see that communities also have some geographical borders. E.g. Europeans tend to work together more often.

For small \(k\) values we find one big interconnected community, along with a few very small communities of about two or three authors. This big connected subgraph is the result of authors who do not have one specific co-author colleague. The only conclusions we can draw from these values is that there are a lot of authors who, once they start writing more papers, do not keep doing so with their very first co-author.

% k = 8
When we take a bigger value for our parameter \(k\), we get more useful communities. We can see more medium sized communities arise from the original huge subgraph.

For intermediate values of \(k\), such as 8, we observe 5 communities. After some research we found some logic behind the communities. The orange and blue communities are generic geographical communities and will be discussed later on with \(k = 11\).

The yellow, red, and green communities came to be because of a single paper each. Every author in these communities is listed together on their paper respectively. We also notice a relation to the authors' background, namely geographical. E.g. yellow consists of authors associated with the Stony Brook University and red consists of authors associated with the TU Delft.

For the red community we note that it is linked to the generic American community (blue) via a single link, namely Peter A. Boncz. They collaborated with Le Gruenwald and Samuel Madden on 1 and 2 papers respectively.

\begin{landscape}
\begin{figure}[!h]
    \label{k8}
    \includegraphics[height=1.0\textheight]{img/k8.png}
    \caption{The k-clique with \(k = 8\).}
\end{figure}
\end{landscape}

For \(k = 11\) we observe two very distinct communities. These are connected mainly by two central authors, namely Dan Suciu and Tova Milo. It does not surprise us to see these two names in the middle. In Subsection \ref{betweenness} we saw that they have a high betweenness rank. Given these two communities we can say that there is a division in the PODS-community. After researching the authors of these communities we can conclude that the red community mainly consists of Americans, while the blue community contains researchers of Europe and western Asia.

Our very own professor Frank Neven is part of one of these communities. In Figure \ref{k11}, we can see him on the left.

\begin{landscape}
\begin{figure}[!h]
    \label{k11}
    \includegraphics[height=1.0\textheight]{img/k11.png}
    \caption{The k-clique with \(k = 11\).}
\end{figure}
\end{landscape}

Our general conclusion for the PODS communities is that they are mainly divided by geographical boundaries. This true as well as on small scale as large scale. I.e. smaller communities are the result of single papers. These usually are the result of authors being associated with the same research institution. Bigger communities are the result of veteran authors who have collaborated with many a other veterans. This causes their communities to expand greatly and be heavily interconnected.

\section{Difficulties}

Our first small problem arose when we had to filter the DBLP dataset for PODS articles. In the end we discovered that the ``booktitle'' XML element specifies the conference, even though sometimes we saw that the PODS-term appeared in the notes XML block. To make sure we got all the papers, we looked for the words `PODS' in both the ``notes'' and ``booktitle'' fields (case insensitive).

Next, for checking whether our code was correct we compared our output to that of the paper. More specifically, by comparing the betweenness scores of 2011. We ended up getting different results. This was because we did not include papers written by PODS authors which were published outside of the PODS conference. This was fixed easily.

\printbibliography

\end{document}
