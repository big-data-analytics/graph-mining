from build_graph import read_graph, show_graph
from networkx import betweenness_centrality
import os

INTERVAL_SIZE = 1
START_YEAR = 1960
END_YEAR = 2020
STR_START_YEAR = str(START_YEAR)

with open("betweenness_centrality.csv", "w") as output_file:
    for stop_year in range(START_YEAR + INTERVAL_SIZE, END_YEAR, INTERVAL_SIZE):
        start_year = START_YEAR
        str_start_year = STR_START_YEAR
        graph = read_graph(start_year, stop_year, False)
        print(start_year, "→", stop_year)
        if graph.number_of_edges() == 0:
            print("No connections. Skipping this era")
            continue

        centrality = betweenness_centrality(graph)
        centrality = sorted(centrality.items(),
                            key=lambda kv: kv[1], reverse=True)
        print("10 most central authors:")
        for i in range(0, 10):
            try:
                print(" - ", centrality[i][0],
                      " (", str(centrality[i][1]), ")", sep="")
            except:
                pass
        print()
        print("Writing full results to file")
        for kv in centrality:
            output_file.write(str_start_year)
            output_file.write(",")
            output_file.write(kv[0])
            output_file.write(",")
            output_file.write(str(kv[1]))
            output_file.write("\n")

        if stop_year < 1985:
            show_graph(graph, str(stop_year))

        _, columns = os.popen('stty size', 'r').read().split()
        print("-" * int(columns))
