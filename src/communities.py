from build_graph import read_graph
from networkx.algorithms.community import k_clique_communities
import matplotlib.pyplot as plt
import os
from networkx import spring_layout
from networkx import draw as networkx_draw
from networkx import draw_networkx_nodes

START_YEAR = 2006
END_YEAR = 2016
K_LIMIT = 15

colors = ["red", "blue", "green", "yellow",
          "orange", "pink", "purple", "violet", "tab:brown", "black", "tab:cyan", "tab:olive", "xkcd:sky blue", "xkcd:hot pink", "xkcd:yellow green", "honeydew", "royalblue", "plum", "tan", "indianred", "tomato"]

with open("communities.csv", "w") as output_file:
    output_file.write("endyear,k,author\n")

    start = START_YEAR
    end = END_YEAR

    print(start, "→", str(end))

    graph = read_graph(START_YEAR, end, False)
    if len(graph.nodes) == 0:
        print("Empty. Skipping")
        exit

    # Calculate the communities
    for k in range(2, K_LIMIT):
        communities = list(k_clique_communities(graph, k))

        a = []
        for community in communities:
            for author in community:
                if not author in a:
                    a.append(author)

        community_graph = graph.subgraph(a)
        positions = spring_layout(community_graph)
        networkx_draw(community_graph, positions,
                      edge_color="lightgrey", with_labels=k > 5,  font_weight='light', node_size=200, width=0.75)

        for (i, community) in enumerate(communities):
            draw_networkx_nodes(community_graph, positions,
                                nodelist=list(community), node_color=colors[i % len(colors)])
        print("There are ", str(len(communities)),
              " communities (k=", k, ").", sep="")
        plt.show()

        #     for author in community:
        #         output_file.write(str(end))
        #         output_file.write(",")
        #         output_file.write(str(k))
        #         output_file.write(",")
        #         output_file.write(author)
        #         output_file.write("\n")
