from sys import exit
import networkx as nx
import matplotlib.pyplot as plt
from matplotlib.figure import SubplotParams


def read_graph(start_year=None, end_year=None, dblp_dataset=False):
    input_file = None
    if dblp_dataset:
        input_file = open("./pods_edges.csv")
    else:
        input_file = open("./all_edges_pods_authors.csv")

    graph = nx.Graph()

    should_check_year = (start_year is not None) or (end_year is not None)

    for line in input_file.readlines():
        data = line.split(",")
        if len(data) != 3:
            print("Warning: this line does not conform to the CSV format:")
            print("\t", data)
            continue

        if should_check_year:
            if data[0] == 'unknown':
                continue
            year = int(data[0])
            if not start_year is None:
                if year < start_year:
                    continue
            if not end_year is None:
                if year > end_year:
                    continue

        graph.add_edge(data[1].strip(), data[2].strip())

    print("Done reading", graph.number_of_edges(),
          "edges over", graph.number_of_nodes(), "points.")

    input_file.close()
    return graph


def show_graph(graph, title="", blocking=True):
    plt.figure(num=title)
    plt.subplots_adjust(left=0.0, bottom=0.0, right=1.0, top=1.0)

    nx.draw_networkx(graph)
    if blocking:
        plt.show()
